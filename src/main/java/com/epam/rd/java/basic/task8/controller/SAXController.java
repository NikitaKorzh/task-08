package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private static final List<Flower> flowers = new ArrayList<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getFlowers() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();

		SAXParser parser = factory.newSAXParser();
		parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, " ");
		parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		XMLHandler handler = new XMLHandler();
		parser.parse(new File(xmlFileName),handler);
		return flowers;
	}

	private  static  class XMLHandler extends DefaultHandler{
		private String name;
		private String soil;
		private String origin;
		private String stemColor;
		private String leafColor;
		private String aveLenFlower;
		private String tempreture;
		private String lightRequiring;
		private String watering;
		private String multiplying;

		private String element;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			element = qName.intern();
			if(element.equals("lighting")){
				lightRequiring = attributes.getValue(0);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String information  = new String(ch,start,length);
			information = information.replace("\n", "").strip();

			if(!information.isBlank()){
				switch (element){
					case "name":
						name = information;
						break;
					case "soil":
						soil = information;
						break;
					case "origin":
						origin = information;
						break;
					case "stemColour":
						stemColor = information;
						break;
					case "leafColour":
						leafColor = information;
						break;
					case "aveLenFlower":
						aveLenFlower = information;
						break;
					case "tempreture":
						tempreture = information;
						break;
					case "watering":
						watering = information;
						break;
					case "multiplying":
						multiplying = information;
						break;
					default:
				}
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			if((nonNull(name)&&!name.isBlank())&&(nonNull(soil)&&!soil.isBlank())&&
					(nonNull(origin)&&!origin.isBlank())&&(nonNull(stemColor)&&!stemColor.isBlank())&&
					(nonNull(leafColor)&&!leafColor.isBlank())&&(nonNull(aveLenFlower)&&!aveLenFlower.isBlank())&&
					(nonNull(tempreture)&&!tempreture.isBlank())&&(nonNull(lightRequiring)&&!lightRequiring.isBlank())&&
					(nonNull(watering)&&!watering.isBlank())&&(nonNull(multiplying)&&!multiplying.isBlank())) {

				Flower flower = new Flower();

				flower.setName(name);
				flower.setSoil(soil);
				flower.setOrigin(origin);
				flower.setStemColor(stemColor);
				flower.setLeafColor(leafColor);
				flower.setAveLenFlower(Integer.parseInt(aveLenFlower));
				flower.setTempreture(Integer.parseInt(tempreture));
				flower.setLighting(lightRequiring);
				flower.setWatering(Integer.parseInt(watering));
				flower.setMultiplying(multiplying);

				flowers.add(flower);
			}

			name = null;
			soil = null;
			origin = null;
			stemColor = null;
			leafColor = null;
			aveLenFlower = null;
			tempreture = null;
			lightRequiring = null;
			watering = null;
			multiplying = null;

		}
	}

}