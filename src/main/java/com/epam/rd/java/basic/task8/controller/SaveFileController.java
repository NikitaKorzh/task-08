package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class SaveFileController {

    private static final String MEASURE = "measure";

    private SaveFileController() {
        throw new IllegalArgumentException();
    }

    public static void writeToXMLFile(List<Flower> list, String path) throws TransformerException, ParserConfigurationException, IOException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument(); // new document file
        Element root = document.createElement("flowers"); // add root element
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        document.appendChild(root);

        for (Flower flower : list) {
            Element elementFlower = document.createElement("flower");
            root.appendChild(elementFlower);

            Element name = document.createElement("name");
            name.setTextContent(flower.getName());
            elementFlower.appendChild(name);

            Element soil = document.createElement("soil");
            soil.setTextContent(flower.getSoil());
            elementFlower.appendChild(soil);

            Element origin = document.createElement("origin");
            origin.setTextContent(flower.getOrigin());
            elementFlower.appendChild(origin);

            Element visualParameters = document.createElement("visualParameters");

            Element stemColor = document.createElement("stemColour");
            stemColor.setTextContent(flower.getStemColor());
            visualParameters.appendChild(stemColor);

            Element leafColor = document.createElement("leafColour");
            leafColor.setTextContent(flower.getLeafColor());
            visualParameters.appendChild(leafColor);

            Element aveLenFlower = document.createElement("aveLenFlower");
            aveLenFlower.setAttribute(MEASURE, "cm");
            aveLenFlower.setTextContent(String.valueOf(flower.getAveLenFlower()));
            visualParameters.appendChild(aveLenFlower);

            elementFlower.appendChild(visualParameters);

            Element growingTips = document.createElement("growingTips");

            Element tempreture = document.createElement("tempreture");
            tempreture.setAttribute(MEASURE, "celcius");
            tempreture.setTextContent(String.valueOf(flower.getTempreture()));
            growingTips.appendChild(tempreture);

            Element lighting = document.createElement("lighting");
            lighting.setAttribute("lightRequiring", flower.getLighting());
            growingTips.appendChild(lighting);

            Element watering = document.createElement("watering");
            watering.setAttribute(MEASURE, "mlPerWeek");
            watering.setTextContent(String.valueOf(flower.getWatering()));
            growingTips.appendChild(watering);

            elementFlower.appendChild(growingTips);

            Element multiplying = document.createElement("multiplying");
            multiplying.setTextContent(flower.getMultiplying());
            elementFlower.appendChild(multiplying);

        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(document);
        FileWriter writer = new FileWriter(path);
        StreamResult result = new StreamResult(writer);
        transformer.transform(source,result);
    }
}
