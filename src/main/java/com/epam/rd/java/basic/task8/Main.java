package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        List<Flower> flow = domController.getFlowers();

        // sort (case 1)
        flow.sort(Comparator.comparing(Flower::getName));

        // save
        String outputXmlFile = "output.dom.xml";
        SaveFileController.writeToXMLFile(flow, outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        List<Flower> flowerSAX = saxController.getFlowers();

        // sort  (case 2)
        flowerSAX.sort(Comparator.comparing(Flower::getName));

        // save
        outputXmlFile = "output.sax.xml";
        SaveFileController.writeToXMLFile(flowerSAX,outputXmlFile);


        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        List<Flower> flowersSTAX = staxController.getFlowers();
        // sort  (case 3)
        flowersSTAX.sort(Comparator.comparing(Flower::getName));
        // save
        outputXmlFile = "output.stax.xml";
        SaveFileController.writeToXMLFile(flowersSTAX,outputXmlFile);
    }


}
