package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {
    private final String xmlFileName;


    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getFlowers() throws IOException, SAXException, ParserConfigurationException {
        List<Flower> flowerList = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD,"");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");


        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(xmlFileName);
        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getElementsByTagName("flower");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Element root = (Element) nodeList.item(i);
            Flower flower = new Flower();

            String[] flowerValues = root.getTextContent().lines().map(String::strip).filter(s -> !s.isBlank()).toArray(String[]::new);
            flower.setName(flowerValues[0]);
            flower.setSoil(flowerValues[1]);
            flower.setOrigin(flowerValues[2]);
            flower.setStemColor(flowerValues[3]);
            flower.setLeafColor(flowerValues[4]);
            flower.setAveLenFlower(Integer.parseInt(flowerValues[5]));
            flower.setTempreture(Integer.parseInt(flowerValues[6]));
            flower.setLighting(root.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent());
            flower.setWatering(Integer.parseInt(flowerValues[7]));
            flower.setMultiplying(flowerValues[8]);
            flowerList.add(flower);
        }

        return flowerList;
    }

}
